#ifndef CUBACPP_ARITY_HH
#define CUBACPP_ARITY_HH

#include <type_traits>

// This header defines the function template cubacpp::arity<F>, which is used
// to determine the number of double arguments with which a callable object
// of type F can be invoked.

namespace cubacpp {
  template <typename F> constexpr int arity();
}

// Implementation details below.

namespace cubacpp {
  namespace detail {

    // Forward declaration for helper functions.
    template <typename F, typename ArgType, int Limit, typename... Args>
    constexpr int nargs_with_type_impl();

    template <typename F, typename T, int Limit = 30>
    constexpr int nargs_with_type();

  } // detail

  // Return the number of arguments of type T, up to limit Limit, with which the
  // callable type F can be invoked. The limit is provided in order to avoid
  // excessive compilation times in the case that F can *not* be invoked with
  // any number of arguments of type T (because, for example, it also takes some
  // argument of a different type).
  template <typename F>
  constexpr int
  arity()
  {
    return detail::nargs_with_type<F, double>();
  }
}

// Helper function implementations below

namespace cubacpp::detail {
  template <typename F, typename T, int Limit>
  constexpr int
  nargs_with_type()
  {
    return nargs_with_type_impl<F, T, Limit + 1>();
  }

  template <typename F, typename ArgType, int Limit, typename... Args>
  constexpr int
  nargs_with_type_impl()
  {
    int result = -1;
    if constexpr (sizeof...(Args) == Limit) {
      // result is already -1
    } else if (std::is_invocable<F, Args...>::value) {
      result = sizeof...(Args);
    } else {
      return nargs_with_type_impl<F,
                                  ArgType,
                                  Limit,
                                  ArgType,
                                  Args...>(); // Add another argument
    }
    return result;
  }

} // cubacpp::detail

#endif
