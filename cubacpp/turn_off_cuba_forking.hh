#ifndef CUBACPP_TURN_OFF_CUBA_FORKING_HH
#define CUBACPP_TURN_OFF_CUBA_FORKING_HH

#include "cuba.h"

namespace cubacpp {
  inline void turn_off_cuba_forking()
  {
    int const ncores = 0;
    int const maxcores = 0;
    cubacores(&ncores, &maxcores);
  }
}

#endif
