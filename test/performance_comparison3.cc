#include "cubacpp/cuhre.hh"
#include "cubacpp/vegas.hh"

#include "performance_helpers.hh"

#include <chrono>
#include <iostream>

double
h2(double u, double v)
{
  // Exact result is from Mathematica 12.1.1.0, integranted symbolically and
  // then evaluated to 25 digits of precision.
  double constexpr k = 0.01890022674239546529975841;
  double const num = 4 * u * u;
  double const term = u - v - 1.0 / 3.0;
  double const denom = term * term + 0.01;
  return k * num / denom;
}

int
main()
{
  cubacpp::turn_off_cuba_forking();
  unsigned long long constexpr maxeval = 100 * 1000 * 1000UL;
  cubacpp::Cuhre cuhre;
  cuhre.maxeval = maxeval;
  cubacpp::Vegas vegas;
  vegas.maxeval = 10 * maxeval;
  cubacpp::IntegrationVolume<2> vol{{-1., -1.}, {1., 1.}};

  std::cout << "# integrand name: h2\n"
      << "# formula: k * 4*u*u / (0.01 + (u-v-1./3.)**2) \n"
      << "# volume: -1 < u < 1, -1 < v < 1 \n";  
  std::cout << "alg\tepsrel\tvalue\terrorest\terror\tneval\tnregions\ttime\n";

  double epsrel = 0.1;
  // warm up the CPU
  auto r = cuhre.integrate(h2, epsrel, 1.0e-12, vol);
  if (r.status != 0)
    return 1;

  using Seconds = std::chrono::duration<double, std::chrono::seconds::period>;
  Seconds max_call_time{60.0};

  cuhre.flags = 0;
  time_in_loop(cuhre, h2, epsrel, 1.0, "cuhre_0", max_call_time, std::cout, vol);

  cuhre.flags = 4;
  time_in_loop(cuhre, h2, epsrel, 1.0, "cuhre_1", max_call_time, std::cout, vol);

  time_in_loop(vegas, h2, epsrel, 1.0, "vegas", max_call_time, std::cout, vol);
}
