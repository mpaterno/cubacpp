#include "cubacpp/cuhre.hh"
#include "cubacpp/vegas.hh"

#include <cmath>
#include <iostream>
#include <limits>

#include "performance_helpers.hh"

using std::cout;
using std::chrono::duration;
using std::chrono::high_resolution_clock;

// From Mathematica 12.1 Integrate, symbolic integration over unit hypercube.
// This is the multilplier that gives fun6 an integrated value of 1 over the
// unit hypercube/
double const fun6_normalization =
  12.0 / (7.0 - 6 * std::log(2.0) * std::log(2.0) + std::log(64.0));

double
fun6(double u, double v, double w, double x, double y, double z)
{
  return fun6_normalization *
         (u * v + (std::pow(w, y) * x * y) / (1 + u) + z * z);
}


// // Call the integration algorithm with the given integrand, and print timing
// // results.
// template <typename ALG, typename F>
// void
// time_and_call(ALG const& a,
//               F f,
//               double epsrel,
//               double correct_answer,
//               char const* algname)
// {
//   using MilliSeconds =
//     std::chrono::duration<double, std::chrono::milliseconds::period>;
//   // We make epsabs so small that epsrel is always the stopping condition.
//   double constexpr epsabs = 1.0e-100;
//   auto t0 = std::chrono::high_resolution_clock::now();
//   auto res = a.integrate(f, epsrel, epsabs);
//   MilliSeconds dt = std::chrono::high_resolution_clock::now() - t0;
//   double absolute_error = std::abs(res.value - correct_answer);
//   bool const good = (res.status == 0);
//   std::cout << std::scientific
//     << std::setprecision(std::numeric_limits<double>::max_digits10)
//     << algname << '\t' << epsrel << '\t';
//   if (good) {
//     std::cout << res.value << '\t' << res.error << '\t' << absolute_error
//               << '\t';
//   } else {
//     std::cout << "NA\tNA\tNA\t";
//   }
//   std::cout << res.neval << '\t' << res.nregions << '\t' << dt.count()
//             << std::endl;
// }

int
main()
{
  cubacpp::turn_off_cuba_forking();
  unsigned long long constexpr maxeval = 100 * 1000 * 1000UL;
  cubacpp::Cuhre cuhre;
  cuhre.maxeval = maxeval;
  cubacpp::Vegas vegas;
  vegas.maxeval = maxeval;

  using Seconds = std::chrono::duration<double, std::chrono::seconds::period>;
  Seconds max_call_time{60.0};

  std::cout << "# integrand name: fun6\n"
      << "# formula: k * (u * v + (std::pow(w, y) * x * y) / (1 + u) + z * z)\n"
      << "# volume: unit\n";
  std::cout << "alg\tepsrel\tvalue\terrorest\terror\tneval\tnregions\ttime\n";

  double epsrel = 1.0e-3;
  // warm up the CPU
  auto r = cuhre.integrate(fun6, epsrel, 1.0e-12);
  if (r.status != 0) return 1;
  
  cuhre.flags = 0;
  time_in_loop(cuhre, fun6, epsrel, 1.0, "cuhre_0", max_call_time, std::cout);
  cuhre.flags = 4;
  time_in_loop(cuhre, fun6, epsrel, 1.0, "cuhre_1", max_call_time, std::cout);
  time_in_loop(vegas, fun6, epsrel, 1.0, "vegas",   max_call_time, std::cout);  
}
