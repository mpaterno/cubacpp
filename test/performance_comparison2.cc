#include "cubacpp/cuhre.hh"
#include "cubacpp/vegas.hh"

#include "performance_helpers.hh"

#include <chrono>
#include <iostream>

double
genz_1abs_5d(double v, double w, double x, double y, double z)
{
  double constexpr k = 0.6371054;
  return abs(cos(4 * v + 5 * w + 6 * x + 7 * y + 8 * z)) / k;
}

int
main()
{
  cubacpp::turn_off_cuba_forking();
  unsigned long long constexpr maxeval = 100 * 1000 * 1000UL;
  cubacpp::Cuhre cuhre;
  cuhre.maxeval = maxeval;
  cubacpp::Vegas vegas;
  vegas.maxeval = maxeval;

  std::cout << "# integrand name: genz_1abs_5d\n"
      << "# formula: abs(cos(4 * v + 5 * w + 6 * x + 7 * y + 8 * z)) / k \n"
      << "# volume: unit\n";  
  std::cout << "alg\tepsrel\tvalue\terrorest\terror\tneval\tnregions\ttime\n";

  double epsrel = 0.1;
  // warm up the CPU
  auto r = cuhre.integrate(genz_1abs_5d, epsrel, 1.0e-12);
  if (r.status != 0)
    return 1;

  using Seconds = std::chrono::duration<double, std::chrono::seconds::period>;
  Seconds max_call_time{60.0};

  cuhre.flags = 0;
  time_in_loop(
    cuhre, genz_1abs_5d, epsrel, 1.0, "cuhre_0", max_call_time, std::cout);

  cuhre.flags = 4;
  time_in_loop(
    cuhre, genz_1abs_5d, epsrel, 1.0, "cuhre_1", max_call_time, std::cout);

  time_in_loop(
    vegas, genz_1abs_5d, epsrel, 1.0, "vegas", max_call_time, std::cout);
}
