#ifndef CUBACPP_TEST_PERFORMANCE_HELPERS_HH
#define CUBACPP_TEST_PERFORMANCE_HELPERS_HH

#include <chrono>
#include <iomanip>
#include <limits>
#include <ostream>

using MilliSeconds =
  std::chrono::duration<double, std::chrono::milliseconds::period>;

struct timing_result {
  bool status;
  MilliSeconds time;
};

template <typename ALG, typename F>
timing_result
time_and_call(ALG const& a,
              F f,
              double epsrel,
              double correct_answer,
              char const* algname,
              std::ostream& os,
              cubacpp::integration_volume_for_t<F> const& vol)
{
  using namespace std;
  using MilliSeconds = chrono::duration<double, chrono::milliseconds::period>;
  // We make epsabs so small that epsrel is always the stopping condition.
  double constexpr epsabs = 1.0e-100;

  // Print out the algorithm name immediately, for better user feedback.
  os << algname << '\t';
  os.flush();

  auto t0 = chrono::high_resolution_clock::now();
  auto res = a.integrate(f, epsrel, epsabs, vol);
  MilliSeconds dt = chrono::high_resolution_clock::now() - t0;

  double absolute_error = abs(res.value - correct_answer);
  bool const good = (res.status == 0);
  os << scientific << setprecision(numeric_limits<double>::max_digits10)
     << epsrel << '\t';
  if (good) {
    os << res.value << '\t' << res.error << '\t' << absolute_error << '\t';
  } else {
    os << "NA\tNA\tNA\t";
  }
  os << res.neval << '\t' << res.nregions << '\t' << dt.count() << endl;
  return {good, dt};
}

template <typename ALG, typename F>
void
time_in_loop(ALG const& a,
             F f,
             double epsrel,
             double correct_answer,
             char const* algname,
             MilliSeconds maxtime,
             std::ostream& os,
             cubacpp::integration_volume_for_t<F> vol = {})
{
  timing_result r;
  do {
    r = time_and_call(a, f, epsrel, correct_answer, algname, os, vol);
    epsrel /= 2.0;
  } while (r.status && (r.time < maxtime) && epsrel > 1.0e-12);
}

#endif